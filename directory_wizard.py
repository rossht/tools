from argparse import ArgumentParser
from os import listdir
from os.path import isfile, isdir, join
import sys
import os
import filecmp
import colorama
from colorama import init, Fore, Back, Style

def isSameFile(filepathA, filepathB):
    if filepathA and filepathB:
        return filecmp.cmp(filepathA, filepathB, shallow=False)
    
def getNumberOfDifferentLines(filepathA, filepathB):
    with open(filepathA, 'r') as fileA:
        with open(filepathB, 'r') as fileB:
            diff = set(fileA).symmetric_difference(fileB)

    diff.discard('\n')

    cnt = 0
    for line in diff:
        cnt = cnt + 1
    
    return cnt

# def intersection(A, B):
#     i, j = 0
#     A.sort()
#     B.sort()
#     ret = []
#     while i < A.length and j < B.length:
#         if isSameFile(A[i], B[j]):
#             ret.append(A[i])
#             i++
#             j++
#         else 


def printDirectoryStructure(src, dest, config):
    if src and not dest:
        print("Printing directory structure for: ")
        print(src)
        print("No destination directory specified")
    if src and dest:
        print("Comparing directory structure for: ")
        print(src)
        print("and: ")
        print(dest)
    print("--------------------------------------------------------------------")
    
    dualDirectoryWalk(src, dest, config, '')

    print("--------------------------------------------------------------------")
    print("done")

dir_pointer_str = '|> '
file_pointer_str = '|- '
match_flag = 'M   '
file_diff_status = 'DIF '
src_only_flag = 'SO  '
dst_only_flag = 'DO  '

def searchFile(file, string):
    
    print("searching: " + file, end='\r')
    f = open(file, errors='ignore')
    line = f.readline()
    while line:
        res = line.find(string)
        if res >= 0:
            print(file + ", line " + str(res))
            print(line)
        
        line = f.readline()
    
    f.close()

def recursiveSearch(dir, search_string):

    for f in listdir(dir):
        next = join(dir, f)
        is_folder = isdir(next)
        
        if is_folder:
            # recurse in to next folder
            recursiveSearch(next, search_string)
        else:
            # exlcude
            filename, file_extension = os.path.splitext(next)
            # if (file_extension == ".tbe" or file_extension == ".jar"):
                # print("exclude: " + next)
                # continue
            # do search
            searchFile(next, search_string)

def singleDirectoryWalk(dir, prefix, config, indent):

    indent += '   '

    for f in listdir(dir):
        next_dir = join(dir, f)
        
        if isdir(next_dir):
            print(prefix + indent + dir_pointer_str + f)
            singleDirectoryWalk(next_dir, prefix, config, indent)
        else:
            if not config["folders_only"]:
                print(prefix + indent + file_pointer_str + f)

def dualDirectoryWalk(src_dir, dest_dir, config, indent):

    dest_filepath = ''
    source_list = []
    dest_list = []
    len_s = 0
    len_d = 0
    if src_dir and isdir(src_dir):
        source_list = listdir(src_dir)
        len_s = len(source_list)
        source_list.sort()
    else:
        src_dir = ''
    if dest_dir and isdir(dest_dir):
        dest_list = listdir(dest_dir)
        len_d = len(dest_list)
        dest_list.sort()
    else:
        dest_dir = ''
    idx_s = 0
    idx_d = 0

    indent += '   '

    while idx_s < len_s or idx_d < len_d:

        src_file = ''
        dest_file = ''
        only_in_src = False
        only_in_dst = False

        if idx_s < len_s:
            src_file = source_list[idx_s]
        if idx_d < len_d:
            dest_file = dest_list[idx_d]

        src_filepath = join(src_dir, src_file)
        dest_filepath = join(dest_dir, dest_file)

        def printAndOrWalk(dir, file, flag):
            if  isdir(dir):
                print(flag + indent + dir_pointer_str + file)
                singleDirectoryWalk(dir, flag, config, indent)
            else:
                if not config["folders_only"]:
                    print(flag + indent + file_pointer_str + file)
        
        if isdir(src_filepath) and isdir(dest_filepath) and src_file == dest_file:
            # both are directories and have the same name
            idx_s += 1
            idx_d += 1
            print(match_flag + indent + dir_pointer_str + src_file)
            dualDirectoryWalk(src_filepath, dest_filepath, config, indent)
        elif isSameFile(src_filepath, dest_filepath):
            idx_s += 1
            idx_d += 1
            if not config["folders_only"]:
                print(match_flag + indent + file_pointer_str + src_file)
        elif src_file and dest_file and src_file == dest_file:
            idx_s += 1
            idx_d += 1
            
            # do a line by line comparison if it is a text file
            d = os.path.splitext(src_file)
            name = d[0]
            ext = d[1]
            
            text_extensions = [".txt", ".csv", ".md", ".py", ".sh", ".bat", ".c", ".cpp", ".h", ".hh", ".hpp", ".php", ".html", ".css", ".sln", ".xml", ".json", ".tex"]

            if ext.lower() in text_extensions:
                num = getNumberOfDifferentLines(src_filepath, dest_filepath)
                print(Back.RED + file_diff_status + indent + file_pointer_str + src_file + " ----- found " + str(num) + " different lines" + Style.RESET_ALL)
            else:
                # haven't detected them as text files, so let's just print out their size difference
                src_size = os.path.getsize(src_filepath)
                dst_size = os.path.getsize(dest_filepath)
                print(Back.RED + file_diff_status + indent + file_pointer_str + src_file + " ----- src size: " + str(src_size) + ", dest size: " + str(dst_size) + Style.RESET_ALL)
        elif src_file and dest_file:
            # not the same file, but both exist
            # since the filenames are sorted alphabetically we do the < operator to compare lexicographic order and determine if file exists in src or dest
            if src_file < dest_file:
                only_in_src = True
            else:
                only_in_dst = True
        elif src_file and not dest_file:
            only_in_src = True
        elif dest_file and not src_file:
            only_in_dst = True
        else:
            return
            
        if only_in_src:
            idx_s += 1
            print(Back.GREEN)
            printAndOrWalk(src_filepath, src_file, src_only_flag)
            print(Style.RESET_ALL)
        if only_in_dst:
            idx_d += 1
            print(Back.CYAN)
            printAndOrWalk(dest_filepath, dest_file, dst_only_flag)
            print(Style.RESET_ALL)
        

def main(args):
    # specify a source directory and a destination directory
    # list differences
    
    # this is the colorama package init call. colorama is used for colouring the terminal output
    init()

    src = args.source_directory
    dest = args.destination_directory
    find_string = args.find_string
    config = {
        "size": args.size,
        "folders_only": args.folders_only
    }
    
    if src and find_string:

        print("Searching for string \"" + find_string + "\" in directory: " + src)
        
        recursiveSearch(src, find_string)
        print("--------------------------------------------------------------------")
        print("done")
    elif src:
        printDirectoryStructure(src, dest, config)
    else:
        parser.print_usage()

parser = ArgumentParser()
parser.add_argument("-s", "--source",
                    action="store", dest="source_directory",
                    help="specify the source directory")
parser.add_argument("-d", "--destination",
                    action="store", dest="destination_directory", 
                    help="specify the destination directory")
parser.add_argument("--folders-only",
                    action="store_true", dest="folders_only", default=False,
                    help="only look at folders, not individual files")
parser.add_argument("--size",
                    action="store_true", dest="size", default=True,
                    help="show differential between source and destination directories")
parser.add_argument("-f", "--find",
                    action="store", dest="find_string",
                    help="search all file contents for this string")

args = parser.parse_args()

if __name__ == "__main__":
    main(args)