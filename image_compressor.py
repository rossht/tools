from argparse import ArgumentParser
import shutil
from os import listdir
from os.path import isfile, isdir, join
import sys
import os
import filecmp
import colorama
from colorama import init, Fore, Back, Style
import subprocess

dir_pointer_str = '|> '
file_pointer_str = '|- '

def singleDirectoryWalk(dir, indent, quality):

    indent += '   '

    for f in listdir(dir):
        current = join(dir, f)
        
        if isdir(current):
            print(indent + dir_pointer_str + f)
            singleDirectoryWalk(current, indent, quality)
        else:
            print(indent + file_pointer_str + f)
            
            d = os.path.splitext(f)
            name = d[0]
            ext = d[1]
            
            image_types = [".jpg", ".png"]
            
            # filter out anything that isn't in the list of image types
            if ext.lower() in image_types:
                #print("detected image! " + current)
            
                # do conversion
                output_filename = name + ext
                output_filepath = os.path.join(dir, output_filename)
                process = subprocess.Popen(['ffmpeg', '-i', current, '-q:v', str(quality), '-y', output_filepath],
                         stdout=subprocess.PIPE, 
                         stderr=subprocess.PIPE,
                         shell=True,
                         universal_newlines=True)
                stdout, stderr = process.communicate()
                #print(stdout)
                if stderr:
                    print(stderr)

def main(args):
    # specify a source directory and a destination directory
    # list differences
    
    # this is the colorama package init call. colorama is used for colouring the terminal output
    init()

    dir = args.target_directory
    qual = int(args.quality)
    
    if not qual or qual < 1 or qual > 30:
        print("ERROR: you need to provide the quality parameter in [1,30]")
        parser.print_usage()
        return
    
    if not dir:
        dir = os.getcwd()
        
    if not isdir(dir):
        print("ERROR: target provided is not a directory. You must specify a target that is a directory.")
        return
    
    # copy the target directory to an "*_original" directory for safe keeping and manual deletion later
    print("dir: " + dir)
    dir = dir.strip("\\")
    dir = dir.strip("/")
    
    head_tail = os.path.split(dir)
    parent = head_tail[0]
    orig_dirname = head_tail[1]
    orig_copy = orig_dirname + "_original"
    orig_copy_path = os.path.join(parent, orig_copy)
    
    shutil.copytree(dir, orig_copy_path)  
    print("Copied original target folder to " + orig_copy_path + ", you must manually delete this when you are finished if you are happy with the results!")
    
    singleDirectoryWalk(dir, '', qual)

parser = ArgumentParser()
parser.add_argument("-t", "--target",
                    action="store", dest="target_directory",
                    help="specify the target directory, if not specified defaults to current directory")
parser.add_argument("-q", "--quality",
                    action="store", dest="quality", 
                    help="specify the compression quality in [1,30] for the ffmpeg -q:v option")

args = parser.parse_args()

if __name__ == "__main__":
    main(args)
    